﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{

    public TurretBlueprint standardTurret;
    public TurretBlueprint rocketTurret;
    public TurretBlueprint laserBeamer;
    public TurretBlueprint plasmaTurret;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardTurret()
    {
        Debug.Log("Purchased");
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectRocketTurret()
    {
        Debug.Log("Purchased Rocket");
        buildManager.SelectTurretToBuild(rocketTurret);
    }

    public void SelectLaserBeamer()
    {
        Debug.Log("Purchased Laser");
        buildManager.SelectTurretToBuild(laserBeamer);
    }
    public void SelectPlasmaTurret()
    {
        Debug.Log("Purchased Laser");
        buildManager.SelectTurretToBuild(plasmaTurret);
    }
}
