﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{

    public static int EnemisAlive = 0;

    public Wave[] waves;

    public Transform spawnPoint;
    public float timeBetweenWaves = 5f;
    public Text waveCountdownText;

    private float countdown = 2.0f;
    private int waveIndex = 0;

    public GameManager gameManager;

    // Update is called once per frame
    void Update()
    {
        // Wait for all enemies to die
        if (EnemisAlive > 0)
        {
            return;
        }

        if (waveIndex >= waves.Length && EnemisAlive <= 0)
        {
            //End level
            gameManager.WinLevel();
            this.enabled = false;
            return;
        }

        if (countdown <= 0.0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }

        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave()
    {
        PlayerStats.Rounds++;

        Wave wave = waves[waveIndex];

        EnemisAlive = wave.count;
        Debug.Log("EnemisAlive = " + EnemisAlive);

        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        waveIndex++;
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
    }
}